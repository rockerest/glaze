import { readFile, writeFile } from "node:fs/promises";

import { build } from "esbuild";
import { globby } from "globby";
import { move } from "fs-extra";

var components = await globby( [
	"./index.js",
	"./demo/fa.js"
] );

await build( {
	"entryPoints": components,
	"allowOverwrite": true,
	"bundle": true,
	"splitting": true,
	"chunkNames": "chunk/[hash]",
	"minify": false,
	"format": "esm",
	"outdir": "./demo/js"
} );

[
	[ "./demo/js/demo/fa.js", "./demo/js/fa.js", "../chunk/", "./chunk/" ]
].forEach( async ( [ from, to, chunks, newChunks ] ) => {
	var content;

	await move( from, to, { "overwrite": true } );

	content = await readFile( to, "utf8" );
	content = content.replace( new RegExp( chunks, "gm" ), newChunks );

	await writeFile( to, content );
} );