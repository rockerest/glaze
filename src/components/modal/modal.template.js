import { html } from "@thomasrandolph/taproot/Component";

export function template( { label } = {} ){
	return html`

	<dialog aria-label="${label}" part="dialog">
		<div close><slot name="close"></slot></div>
		<slot></slot>
	</dialog>

	`;
}
