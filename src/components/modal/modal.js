import { Base } from "../Base.js";

import { template } from "./modal.template.js";
import { styles } from "./modal.style.js";

export class Modal extends Base{
	static name = "modal";
	static register(){
		Base.register( this.name, this );
	}

	static get styles(){
		return [
			styles()
		];
	}
	static get properties(){
		return {
			"show": { "type": Boolean, "reflect": true },
			"label": { "type": String, "reflect": true },
		};
	}

	get observe(){
		return {
			"show": ( oldShow, newShow ) => {
				if( newShow ){
					this.open();
				}
				else{
					this.close();
				}
			}
		};
	}

	constructor( ...args ){
		super( ...args );

		this.show = false;
		this.label = "";

		// Internal
		this.dialog = null;
	}

	render(){
		return template( {
			"label": this.label
		} );
	}
	firstUpdated(){
		super.firstUpdated();

		this.dialog = this.shadowRoot.querySelector( "dialog" );
	}

	open(){
		this.dialog?.showModal();

		this.emit( `${Modal.registeredName}-open` );
	}
	close(){
		this.dialog?.close();

		this.emit( `${Modal.registeredName}-close` );
	}
}
