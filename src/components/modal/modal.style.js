import { css } from "@thomasrandolph/taproot/Component";

export function styles(){
	return css`

	@layer component.base, component.state, component.theme;

	@layer component{
		@layer base{
			:host{
				display: none;
				height: 100%;
				left: 0;
				position: absolute;
				top: 0;
				width: 100%;
			}

			:host([show]){
				display: block;
			}

			dialog{
				border: 0;
			}

			dialog::backdrop{
				background-color: hsla( 0, 0%, 25%, 0.75 );
			}

			[close]{
				position: absolute;
				right: 0;
				top: 0;
			}
		}

		@layer state{}

		@layer theme{}
	}


	`;
}
