import { css } from "@thomasrandolph/taproot/Component";

export function styles(){
	return css`

:host{
	display: block;
	left: 0;
	position: absolute;
	top: 0;
}

.hide{
	display: none;
}

.popover{
	max-width: 90vw;
	z-index: 1
}

.popover ::slotted( * ){
	position: relative;
	z-index: 9001;
}

`;
}
