import { html, classMap } from "@thomasrandolph/taproot/Component";

export function template( {
	hidden,
	popoverClasses
} ){
	return html`

<div class="popover ${classMap( popoverClasses )}" aria-hidden="${hidden}">
	<slot>Popover Slot Default</slot>
</div>

`;
}

