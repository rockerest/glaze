import {
	computePosition,
	autoUpdate,
	offset,
	shift,
	flip
} from "@floating-ui/dom";

import { Base } from "../Base.js";

import { template } from "./popover.template.js";
import { styles } from "./popover.style.js";

function shouldAutomaticallyClose( component ){
	return component.blurClose && component.visible;
}

function listenOutside( component, event ){
	var path = event.composedPath().filter( ( node ) => node != document.body );
	var ignore = [
		component,
		component.attach,
		...component.considerInside
	];
	var close;

	if( shouldAutomaticallyClose( component ) ){
		close = ignore.reduce( ( decision, ignoredNode ) => decision && !path.includes( ignoredNode ), true );
	}

	if( close ){
		component.visible = false;
	}
}

export class Popover extends Base{
	static name = "popover";
	static register(){
		Base.register( this.name, this );
	}

	static get properties(){
		return {
			"attach": { "type": Object },
			"visible": { "type": Boolean, "reflect": true },
			"placement": { "type": String, "reflect": true },
			"blurClose": { "type": Boolean, "reflect": true, "attribute": "blur-close" },
			"considerInside": { "type": Array, "attribute": "consider-inside" },
			"offsets": { "type": Object }
		};
	}
	static get styles(){
		return [
			styles()
		];
	}

	get observe(){
		return {
			"visible": ( oldVisible, newVisible ) => {
				this.redraw();
				this.emit( `${Popover.registeredName}-visibility`, newVisible );
			},
			"attach": () => {
				this.redraw();
			},
			"placement": () => {
				this.redraw();
			},
			"offsets": () => {
				this.redraw();
			}
		};
	}

	constructor( ...args ){
		super( ...args );

		this.attach = document.body;
		this.visible = false;
		this.blurClose = false;
		this.considerInside = [];
		this.offsets = {
			"distance": 0,
			"skidding": 0
		};
		this.placement = "bottom";

		this.outsideListener = ( event ) => listenOutside( this, event );
		this.removeAutoUpdater = null;
	}
	connectedCallback( ...args ){
		super.connectedCallback( ...args );

		if( !this.attach ){
			this.attach = document.body;
		}

		this.bind();
	}
	disconnectedCallback( ...args ){
		this.unbind();

		super.disconnectedCallback( ...args );
	}

	render(){
		return template( {
			"hidden": this.visible ? "false" : "true",
			"popoverClasses": {
				"hide": !this.visible
			}
		} );
	}
	firstUpdated(){
		this.redraw();
	}

	async redraw(){
		var positioning;

		if( this.attach ){
			positioning = await computePosition(
				this.attach,
				this,
				{
					"placement": this.placement,
					"middleware": [
						offset( {
							"mainAxis": this.offsets.distance,
							"crossAxis": this.offsets.skidding
						} ),
						shift(),
						flip()
					]
				}
			);

			Object.assign( this.style, {
				"left": `${positioning.x}px`,
				"top": `${positioning.y}px`
			} );
		}
	}
	bind(){
		document.addEventListener( "click", this.outsideListener );

		this.removeAutoUpdater = autoUpdate( this.attach, this, () => this.redraw() );
	}
	unbind(){
		if( this.removeAutoUpdater ){
			this.removeAutoUpdater();
		}

		document.removeEventListener( "click", this.outsideListener );
	}
}
