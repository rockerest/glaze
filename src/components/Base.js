import { Component } from "@thomasrandolph/taproot/Component";

export class Base extends Component{
	static library = "glaze";
	static register( name, component ){
		if( !customElements.get( `${this.library}-${name}` ) ){
			customElements.define( `${this.library}-${name}`, component );
		}
	}
	static get registeredName(){
		var name;

		if( this.name != "Base" ){
			name = `${this.library}-${this.name}`;
		}

		return name;
	}
}