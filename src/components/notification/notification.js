import { Base } from "../Base.js";

import { template } from "./notification.template.js";
import { styles } from "./notification.style.js";

export class Notification extends Base{
	static name = "notification";
	static register(){
		Base.register( this.name, this );
	}

	static get styles(){
		return [
			styles()
		];
	}
	static get properties(){
		return {
			"type": { "type": String, "reflect": true },
			"dismissible": { "type": Boolean, "reflect": true },
			"centered": { "type": Boolean, "reflect": true },
			"icon": { "type": Boolean, "reflect": true },
			"inline": { "type": Boolean, "reflect": true }
		};
	}

	constructor( ...args ){
		super( ...args );

		this.type = "neutral";
		this.dismissible = false;
		this.centered = false;
		this.icon = false;
		this.inline = false;
	}

	render(){
		return template( {
			"icon": this.icon,
			"dismissible": this.dismissible
		} );
	}

	dismiss(){
		this.parentNode.removeChild( this );
	}
}