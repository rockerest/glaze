import { html, when } from "@thomasrandolph/taproot/Component";

export function template( { icon, dismissible } = {} ){
	return html`

	${when(
		icon,
		() => html`<slot name="icon" part="icon"></slot>`
	)}
	<div class="content" part="content">
		<slot></slot>
	</div>
	${when(
		dismissible,
		() => html`<slot name="close" part="close"></slot>`
	)}

	`;
}
