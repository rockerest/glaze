import { css } from "@thomasrandolph/taproot/Component";

export function styles(){
	return css`

:host{
	--color-border: hsla( 0, 0%, 68.6%, 0.35 );
	--color-subtle: hsla( 0, 0%, 94.1%, 1 );
	--color-info: hsla( 212.3, 60.6%, 49.8%, 1 );
	--color-okay: hsla( 120.7, 40.6%, 39.6%, 1 );
	--color-attention: hsla( 41.1, 68.2%, 58%, 1 );
	--color-alert: hsla( 0, 55.6%, 46.9%, 1 );
	
	--size-padding: 1rem;
	--size-padding-small: 0.5rem;
	--size-border-radius: 3px;

	background-color: var( --color-subtle );
	border: 1px solid var( --color-border );
	border-block-start-width: 3px;
	border-radius: var( --size-border-radius );
	display: grid;
	gap: var( --size-padding-small );
	grid-template-columns: 1fr;
	justify-items: start;
	padding: var( --size-padding ) var( --size-padding-small );
}

:host([inline]){
	border-width: 0 0 0 3px;
	padding: 0 var( --size-padding-small ) 0;
}

:host([centered]){
	justify-items: center;
	text-align: center;
}

:host([dismissible]){
	grid-template-columns: 1fr min-content;
}
:host([icon]){
	grid-template-columns: min-content 1fr;
}
:host([icon][dismissible]){
	grid-template-columns: min-content 1fr min-content;
}
:host([type="neutral"]) [name="icon"]{
	color: inherit;
}

:host([type="info"]){
	border-block-start-color: var( --color-info );
}
:host([type="info"][inline]){
	border-inline-start-color: var( --color-info );
}
:host([type="info"]) [name="icon"]{
	color: var( --color-info );
}

:host([type="okay"]){
	border-block-start-color: var( --color-okay );
}
:host([type="okay"][inline]){
	border-inline-start-color: var( --color-okay );
}
:host([type="okay"]) [name="icon"]{
	color: var( --color-okay );
}

:host([type="attention"]){
	border-block-start-color: var( --color-attention );
}
:host([type="attention"][inline]){
	border-inline-start-color: var( --color-attention );
}
:host([type="attention"]) [name="icon"]{
	color: var( --color-attention );
}

:host([type="alert"]){
	border-block-start-color: var( --color-alert );
}
:host([type="alert"][inline]){
	border-inline-start-color: var( --color-alert );
}
:host([type="alert"]) [name="icon"]{
	color: var( --color-alert );
}

	`;
}
