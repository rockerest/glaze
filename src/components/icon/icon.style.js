import { dom } from "@fortawesome/fontawesome-svg-core";
import { css, unsafeCSS } from "@thomasrandolph/taproot/Component";

export function fa(){
	return css`${unsafeCSS( dom.css() )}`;
}

export function styles(){
	return css`

	:host{
		box-sizing: border-box;
		display: inline-block;
	}

	:host *{
		box-sizing: inherit;
	}

	span.icon{
		display: inline-block;
		line-height: 1em;
	}

	`;
}
