import { html, unsafeHTML } from "@thomasrandolph/taproot/Component";

export function template( { markupList } = {} ){
	var allMarkup = markupList.join( "\n" );

	return html`
<span class="icon">${unsafeHTML( allMarkup )}</span>
`;
}
