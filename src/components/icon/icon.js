import { library, icon, parse } from "@fortawesome/fontawesome-svg-core";

import { Base } from "../Base.js";

import { template } from "./icon.template.js";
import { fa, styles } from "./icon.style.js";

function getClasses( { spin, fixedWidth, sharp } = {} ){
	var list = {
		"fa-spin": spin,
		"fa-fw": fixedWidth,
		"fa-sharp": sharp
	};

	return Object
		.entries( list )
		.reduce( ( classes, [ name, on ] ) => {
			let classList = [ ...classes ];

			if( on ){
				classList.push( name );
			}

			return classList;
		}, [] );
}

function getSvg( { type, "icon": iconName, transforms, title, classes } = {} ){
	var packs = {
		"thin": "fat",
		"light": "fal",
		"brands": "fab",
		"solid": "fas",
		"regular": "far",
		"duotone": "fad",
		"duotone-sharp": "fasds"
	};
	var svg = {
		"prefix": packs[ type ],
		iconName
	};
	var compiled = icon( svg, {
		"transform": parse.transform( transforms ),
		"attributes": {
			"part": "svg"
		},
		title,
		classes
	} );

	if( !compiled ){
		throw new Error( `The type '${type}' and name '${iconName}' did not result in a usable icon definition.` );
	}

	return compiled?.html;
}

export class Icon extends Base{
	static name = "icon";
	static register( definitions = [] ){
		Icon.addDefinitions( definitions );

		Base.register( this.name, this );
	}
	static addDefinitions( definitions ){
		library.add( ...definitions );
	}
	static get fa(){
		return library;
	}

	static get styles(){
		return [
			fa(),
			styles()
		];
	}
	static get properties(){
		return {
			"type": { "type": String, "reflect": true },
			"sharp": { "type": Boolean, "reflect": true },
			"spin": { "type": Boolean, "reflect": true },
			"fixedWidth": { "type": Boolean, "reflect": true, "attribute": "fixed-width" },
			"focusable": { "type": Boolean, "reflect": true },
			"title": { "type": String, "reflect": true },
			"icon": { "type": String, "reflect": true },
			"transforms": { "type": String, "reflect": true }
		};
	}

	constructor(){
		super();

		this.type = "solid";
		this.sharp = false;
		this.spin = false;
		this.fixedWidth = false;
		this.focusable = false;
		this.title = "";
		this.icon = "";
		this.transforms = "";
	}

	firstUpdated(){
		if( this.focusable ){
			this.setAttribute( "tabindex", 0 );
		}
	}

	render(){
		return template( {
			"markupList": getSvg( {
				"type": this.type,
				"icon": this.icon,
				"transforms": this.transforms,
				"title": this.title,
				"classes": getClasses( {
					"spin": this.spin,
					"fixedWidth": this.fixedWidth,
					"sharp": this.sharp
				} )
			} )
		} );
	}
}