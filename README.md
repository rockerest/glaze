## Glaze Components

+++++++++++++++++++++++++
### Icon
+++++++++++++++++++++++++

#### Attributes and Properties

| Name | Attribute (name) | Property | Type | Optional | Reason |
| ---- | ---------------- | -------- | ---- | -------- | ------ |
| `type`		| ✅ (`type`)		| ✅ | String	| ✅ | Which FontAwesome Classic style to use. Defaults to `"solid"`. |
| `icon`		| ✅ (`icon`)		| ✅ | String	| ✅ | Which [icon](https://fontawesome.com/icons) to display. |
| `sharp`		| ✅ (`sharp`)		| ✅ | Boolean	| ✅ | Whether to use the FontAwesome Pro [Sharp style](https://blog.fontawesome.com/introducing-font-awesome-sharp/). |
| `spin`		| ✅ (`spin`)		| ✅ | Boolean	| ✅ | Whether to spin this icon. |
| `fixedWidth`	| ✅ (`fixed-width`)	| ✅ | Boolean	| ✅ | Whether to render this icon as a fixed width. |
| `focusable`	| ✅ (`focusable`)	| ✅ | Boolean	| ✅ | Whether this icon is focusable via keyboard navigation. |
| `title`		| ✅ (`title`)		| ✅ | String	| ✅ | An alternate title to render for accessibility. |
| `transforms`	| ✅ (`transforms`)	| ✅ | String	| ✅ | A string of [power transforms](https://fontawesome.com/docs/web/style/power-transform) to apply to this icon. |

Note that while `icon` is technically optional, you will very likely have a very bad time with a blank icon.  
Additionally, note that `title` is an accessibility feature and while it is an optional property, it should be included.

#### `#register`

| Parameter | Reason | Optional |
| --------- | ------ | -------- |
| `definitions` | Provide FontAwesome icon definitions for the Icon component to reference | ✅ |

`register` defines the `glaze-icon` custom element so it's available to use.  

`definitions` provide a shortcut for registering FontAwesome icon definitions for the Icon component to use.

#### `#addDefinitions`

| Parameter | Reason | Optional |
| --------- | ------ | -------- |
| `definitions` | Provide FontAwesome icon definitions for the Icon component to reference | ✅ |

If the component has already been registered with the custom element registry, `addDefinitions` can update the list of available icons to use at any time.

----

+++++++++++++++++++++++++
### Modal
+++++++++++++++++++++++++

#### Attributes and Properties

| Name | Attribute (name) | Property | Type | Optional |Reason |
| ---- | ---------------- | -------- | ---- | -------- |------ |
| `show`				| ✅ (`show`)					| ✅ | Boolean	| ✅ | Whether the modal is visible or not. |
| `label`				| ✅ (`label`)					| ✅ | String	| ✅ | A text label to add to the dialog element for accessibility purposes. |

Note that `label` is an accessibility feature and while it is an optional property, it should be included.

#### `#register`

`register` defines the `glaze-modal` custom element so it's available to use.

#### Events

| Name | Payload | When |
| ---- | ------- | ---- |
| `glaze-modal-open`	| null | When the `show` property is changed, and becomes `true`. |
| `glaze-modal-close`	| null | When the `show` property is changed, and becomes `false`. |

#### Slots

|   Name  | Use |
| ------- | --- |
| default | This is the content that is displayed in the modal body. |
| close   | This is the content that is displayed in the top right corner of the modal by default. |

#### CSS Parts

|   Name  | Use |
| ------- | --- |
| close   | Can be used to target the close button slot that is normally positioned in the top right corner of the modal. |

----

+++++++++++++++++++++++++
### Notification
+++++++++++++++++++++++++

#### Attributes and Properties

| Name | Attribute (name) | Property | Type | Optional | Reason |
| ---- | ---------------- | -------- | ---- | -------- | ------ |
| `type`		| ✅ (`type`)		| ✅ | String	| ✅ | Which notification type to display. Options are `neutral` (equivalent to no type at all), `info`, `okay`, `attention`, and `alert`. |
| `dismissible`	| ✅ (`dismissible`)	| ✅ | Boolean	| ✅ | Whether to render the `close` slot |
| `centered`	| ✅ (`centered`)	| ✅ | Boolean	| ✅ | Whether to center the text of this notification. |
| `banner`		| ✅ (`banner`)		| ✅ | Boolean	| ✅ | Whether to display as a banner, which has fixed position and width. |
| `icon`		| ✅ (`icon`)		| ✅ | Boolean	| ✅ | Whether to render the `icon` slot |
| `inline`		| ✅ (`inline`)		| ✅ | Boolean	| ✅ | Whether to display as a very minimal output. |

#### `#register`

`register` defines the `glaze-notification` custom element so it's available to use.

#### `#createBanner`

`createBanner` creates a new Notification component without needing to render it directly into an HTML template.

| Parameter | Reason | Optional |
| --------- | ------ | -------- |
| `options` | How and what to render for this Notification | ❌ |

`options` defaults to:

```javascript
{
	"type": "neutral",
	"content": undefined,
	"centered": true,
	"dismissible": true,
	"icon": true
}
```

These `options` properties map directly to the attributes and properties of the same names, with the exception of `content`.

`options.content` is a required property (unless you would like to render an empty banner notification), and is rendered as a direct child of the Notification (e.g. `<Notification>${options.content}</Notification>`).

#### Slots

|   Name  | Use |
| ------- | --- |
| icon    | This is a left-side slot that could be very useful to render an icon with the notification. |
| default | This is the content that is displayed in the notification body. |
| close   | This is a right-side slot that could be very useful to render an close icon with the notification to dismiss it. |

#### CSS Parts

|   Name  | Use |
| ------- | --- |
| icon    | Can be used to target the `icon` slot if it's rendered. |
| content | Can be used to target the `div` that wraps the main `content` slot. |
| close   | Can be used to target the `close` slot if it's rendered. |

+++++++++++++++++++++++++
### Popover
+++++++++++++++++++++++++

#### Attributes and Properties

|     Name    | Attribute (name)  | Property |   Type  | Optional | Reason |
| ----------- | ----------------- | -------- | ------- | -------- | ------ |
| `attach`    | ❌                | ✅       | Object  | ✅ | The DOM element to attach the popover to. Defaults to `document.body`. |
| `visible`   | ✅ (`visible`)    | ✅       | Boolean | ✅ | Whether this popover should be visible or not. Defaults to `false`. |
| `placement` | ✅ (`placement`)  | ✅       | String  | ✅ | Any of the values defined by [floating-ui](https://floating-ui.com/). For example: `right-end`. |
| `blurClose` | ✅ (`blur-close`) | ✅       | Boolean | ✅ | Whether a click outside the component should close the component. Note that the event listener is bound regardless, but only causes behavior when `blurClose` is `true`. |
| `considerInside` | ✅ (`consider-inside`) | ✅ | Array | ✅ | A list of additional nodes to ignore when computing whether a click has occurred "outside" the popover. Normally, only the popover and its `.attach` are considered "inside". This list allows more nodes that will be added to that list. |
| `offsets`   | ✅ (`offsets`)    | ✅       | Object  | ✅ | An object with the properties `skidding` and `distance`. Skidding moves the popover along the edge of the `attach` element (parallel), while `distance` moves the popover away from the edge of the `attach` element (perpendicular). See the [axesOffsets of the Offset middleware](https://floating-ui.com/docs/offset#axesoffsets) for more information. Neither value is optional if any value is provided. |

#### `#register`

`register` defines the `glaze-popover` custom element so it's available to use.

#### Events

|             Name           | Payload | When |
| -------------------------- | ------- | ---- |
| `glaze-popover-visibility` | Boolean | When the `visible` property is changed. |

#### Slots

|   Name  | Use |
| ------- | --- |
| default | This is the content that is displayed in the popover. |