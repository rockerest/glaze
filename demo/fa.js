import { far } from "@fortawesome/free-regular-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";

export var definitions = [
	...Object.values( far ),
	...Object.values( fas ),
	...Object.values( fab )
];